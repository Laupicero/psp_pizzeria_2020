package com.lauralucena.pizzeria.pojos;

import com.lauralucena.pizzeria.interfaces.PizzaTemp;
import java.util.List;
import java.io.Serializable;

public class Pizza extends Producto implements PizzaTemp{
    private String nombrePizza;
    private String tamPizza;
    private int imgLocal;
    private List<Ingrediente> ingredientesPizza;


    //Constructor
    public Pizza(String nombrePizza, String tamPizza, int img ,List<Ingrediente> ingredientesPizza){
        this.nombrePizza = nombrePizza;
        this.tamPizza = tamPizza;
        this.imgLocal = img;
        this.ingredientesPizza = ingredientesPizza;
    }

    //MÉTODOS
    //Métodos SETTER & GETTER
    //Set
    public void setNombrePizza(String nombrePizza){this.nombrePizza = nombrePizza;}
    public void setTamPizza(String tam){this.tamPizza = tam;}
    public void setImgLocal(int img){this.imgLocal = img;}
    public void setListaIngredientesPizza(List<Ingrediente> ingredientesPizza){ this.ingredientesPizza = ingredientesPizza;}
    //Get
    public String getNombrePizza(){  return this.nombrePizza;}
    public  String getTamPizza(){return this.tamPizza;}
    public int getimgLocal(){return this.imgLocal;}
    public List<Ingrediente> getListaIngredientesPizza(){ return this.ingredientesPizza;}


    @Override
    public double getPrecioPizza() {
        double precioPizza = 0;

        if(this.tamPizza.equals("Mediana"))
            precioPizza = 2;
        else
            precioPizza = 3.5;

        for(int i=0; i < ingredientesPizza.size(); i++){
            precioPizza += ingredientesPizza.get(i).getPrecioIngrediente();
        }
        return precioPizza;
    }

    public String getIngredientesStringFromPizza(){
        String ingredientes = "";
        for(Ingrediente ingred : this.ingredientesPizza)
            ingredientes += ingred.getNombreIngrediente() + " ";
        return ingredientes;
    }
}
