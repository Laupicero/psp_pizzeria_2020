package com.lauralucena.pizzeria.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.controllers.ControladorDAO;
import com.lauralucena.pizzeria.pojos.Pizza;

import java.util.List;

public class PizzaAdapter extends RecyclerView.Adapter<PizzaAdapter.ViewRow> {

    private final List<Pizza> pizzaList;
    private OnClickPizzaListener listener;

    public interface OnClickPizzaListener{
        void onItemClicked(Pizza item);
    }

    public PizzaAdapter(List<Pizza> pizzaList){
        this.pizzaList = pizzaList;
    }

    public void setListener(OnClickPizzaListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewRow onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reciclerviewrow_pizza, parent, false);

        return new ViewRow(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewRow holder, int position) {
        holder.bind(pizzaList.get(position));
    }

    @Override
    public int getItemCount() {
        return this.pizzaList.size();
    }

    public class ViewRow extends RecyclerView.ViewHolder {
        private final TextView tvNombre;
        private final TextView tvIngredientes;
        private final ImageView imgViewPizza;
        private final LinearLayout llPadre;

        public ViewRow(View view) {
            super(view);
            // Define click listener for the ViewHolder's View

            tvNombre = view.findViewById(R.id.tvPizzaName);
            tvIngredientes = view.findViewById(R.id.tvPizzaInredients);
            imgViewPizza = view.findViewById(R.id.imgvPizza);
            llPadre = view.findViewById(R.id.llPadre);
        }

        public void bind(Pizza pizza){
            tvNombre.setText(pizza.getNombrePizza());
            tvIngredientes.setText(ControladorDAO.getIngredientesFromAPizza(pizza));
            imgViewPizza.setImageDrawable(ContextCompat.getDrawable(itemView.getContext(), pizza.getimgLocal()));
            llPadre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(listener != null){
                        listener.onItemClicked(pizza);
                    }
                }
            });
        }
    }
}
