package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.pojos.Ingrediente;
import com.lauralucena.pizzeria.pojos.Pizza;

import java.util.ArrayList;
import java.util.List;

public class PizzaPersonalizadaActivity extends BaseActivity {
    private List<Ingrediente> lstIngredientes;

    private Button btnConfirmarPedido, btnAtras;

    private CheckBox cbBacon, cbChampi, cbJamon, cbQueso,
            cbBbq, cbTernera, cbMozarella, cbNata,
            cbHuevo, cbPimiento, cbSalchichas, cbCebolla, cbMarcarFavorita;

    //private RadioGroup rgTamPizzas;
    //private RadioButton rbPizzaMediana, rbPizzaFamiliar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_personalizada);

        //Lista Ingredientes
        lstIngredientes = new ArrayList<>();

        //BINDEAMOS
        this.btnConfirmarPedido = findViewById(R.id.btnConfirmarPedido);
        this.btnAtras = findViewById(R.id.btnAtras);
        // RADIO - BUTTON
        //rgTamPizzas = findViewById(R.id.rgTamPizza);
        //rbPizzaMediana = findViewById(R.id.rbMediana);
        //rbPizzaFamiliar = findViewById(R.id.rbFamiliar);
        //LOS Check-Box Ingredientes
        cbBacon = findViewById(R.id.cbBacon);
        cbChampi = findViewById(R.id.cbChampi);
        cbJamon = findViewById(R.id.cbjamon);
        cbQueso = findViewById(R.id.cbqueso);
        cbBbq = findViewById(R.id.salsaBbq);
        cbTernera = findViewById(R.id.cbTernera);
        cbMozarella = findViewById(R.id.cbmozarella);
        cbNata = findViewById(R.id.cbNata);
        cbHuevo = findViewById(R.id.cbHuevo);
        cbPimiento = findViewById(R.id.cbPimiento);
        cbSalchichas = findViewById(R.id.cbSalchichas);
        cbCebolla = findViewById(R.id.cbCebolla);


        //----------------------------
        // EVENTOS INGREDIENTES
        //----------------------------
        //Añadimos los ingredientes a una lista de ingredientes
        //BACON
        cbBacon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbBacon.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbBacon.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbBacon);}
            }
        });
        //CHAMPIÑONES
        cbChampi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbChampi.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbChampi.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbChampi);}
            }
        });
        //JAMÓN
        cbJamon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbJamon.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbJamon.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbJamon);}
            }
        });
        //QUESO
        cbQueso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbQueso.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbQueso.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbQueso);}
            }
        });
        //BBQ
        cbBbq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbBbq.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbBbq.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbBbq);}
            }
        });
        //TERNERA
        cbTernera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbTernera.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbTernera.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbTernera);}
            }
        });
        //MOZARELLA
        cbMozarella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbMozarella.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbMozarella.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbMozarella);}
            }
        });
        //NATA
        cbNata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbNata.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbNata.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbNata);}
            }
        });
        //HUEVO
        cbHuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbHuevo.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbHuevo.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbHuevo);}
            }
        });
        //PIMIENTO
        cbPimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbPimiento.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbPimiento.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbPimiento);}
            }
        });
        //SALCHICHAS
        cbSalchichas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbSalchichas.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbSalchichas.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbSalchichas);}
            }
        });
        //CEBOLLA
        cbCebolla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbCebolla.isChecked()){
                    Ingrediente i1 = new Ingrediente(cbCebolla.getText().toString());
                    lstIngredientes.add(i1);

                }else{lstIngredientes.remove(cbCebolla);}
            }
        });


        //--------------
        // Botón confirmar pedido
        //--------------
        this.btnConfirmarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pizza pizzaPedido = new Pizza("Personalizada", "Mediana", R.drawable.margarita, lstIngredientes);

                Intent actFinPedido = new Intent(PizzaPersonalizadaActivity.this, PizzaDataActivity.class);
                Bundle bundPizza = new Bundle();
                bundPizza.putSerializable("pedidoPizza", pizzaPedido);;
                actFinPedido.putExtras(bundPizza);
                startActivity(actFinPedido);
                finish();
            }
        });


        //--------------
        // Botón Atrás
        //--------------
        this.btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PizzaPersonalizadaActivity.this, PedidosActivity.class);
                startActivity(intent);
                finish();
            }
        });


        /*
        * Si el tamaño de las pizzas se hiciera con RadioButton

        rgTamPizzas.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(rbPizzaMediana.isChecked()){
                    TamPizza = "Mediana";
                }else if(rbPizzaFamiliar.isChecked()){
                    TamPizza = "Familiar";
                }else {
                    Context context = getApplicationContext();
                    CharSequence text = "Seleccione un tamaño para su Pizza";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
        * */
    }
}