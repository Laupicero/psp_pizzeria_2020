package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.chip.ChipGroup;
import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.controllers.ControladorDAO;
import com.lauralucena.pizzeria.pojos.Pizza;

public class PizzaDataActivity extends BaseActivity {
    private Button btnConfirmaredido, btnAtras;
    private TextView tvTitlePizza, tvIngredientes;
    private CheckBox cbTamFamiliar, cbFavorita;
    private ImageView pizzaImg;
    private Pizza pedidoPizza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_details);

        this.btnConfirmaredido = findViewById(R.id.btnConfirmarPedido);
        this.btnAtras = findViewById(R.id.btnAtras);
        this.tvTitlePizza = findViewById(R.id.titlePizza);
        this.tvIngredientes = findViewById(R.id.tvIngredientes);
        this.cbTamFamiliar = findViewById(R.id.cbTamFamiliar);
        this.cbFavorita = findViewById(R.id.cbFavorita);
        this.pizzaImg = findViewById(R.id.pizzaImg);

        //Obtenemos el Bundle
        this.pedidoPizza = (Pizza) getIntent().getSerializableExtra("pedidoPizza");  // NO (Pizza) savedInstanceState.getSerializable("pedidoPizza");

        if(pedidoPizza !=null){
            this.tvTitlePizza.setText(pedidoPizza.getNombrePizza());
            this.tvIngredientes.setText(pedidoPizza.getIngredientesStringFromPizza());
            this.pizzaImg.setBackgroundResource(pedidoPizza.getimgLocal());

            if(this.cbTamFamiliar.isChecked())
                this.pedidoPizza.setTamPizza("Familiar");
        }

        //--------------------
        // Seleccionamos el Tamaño Familiar
        //--------------------
        this.cbTamFamiliar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                pedidoPizza.setTamPizza("Familiar");
            }
        });

        //--------------------
        // Guarda Pizza Favorita
        //--------------------
        cbFavorita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(cbFavorita.isChecked()){
                    ControladorDAO.guardarPizzaFavorita(getApplicationContext(), pedidoPizza);
                }
            }
        });


        //--------------------
        //Botón para confirmar pedido
        //--------------------
        this.btnConfirmaredido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PizzaDataActivity.this, ConfirmacionPedidoActivity.class);
                intent.putExtra("pedidoPizza",  pedidoPizza);
                startActivity(intent);
                finish();
            }
        });


        //--------------------
        //Botón para volver a pedidos
        //--------------------
        this.btnAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                Intent intent = new Intent(PizzaDataActivity.this, PedidosActivity.class);
                startActivity(intent);
            }
        });
    }
}