package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lauralucena.pizzeria.R;

public class MenuActivity extends BaseActivity {
    private Button btnPedido, btnWeb, btnConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        this.btnConfig = findViewById(R.id.btnConfig);
        this.btnPedido = findViewById(R.id.btnPedidos);
        this.btnWeb = findViewById(R.id.btnWeb);


        // Evento para ir a la ventana de Pedido
        this.btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, PedidosActivity.class);
                startActivity(intent);
            }
        });


        // Evento para ir a la página Web
        this.btnWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://marruzella.es/"));
                startActivity(intent);
            }
        });


        // Evento para abir la ventana de configuración
        this.btnConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ConfigActvity.class);
                startActivity(intent);
            }
        });
    }

    // Para sobreEscribir el botón atrás
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);
        //Botón SI
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                finishAffinity();
            }
        });
        //Botón NO
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}