package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.controllers.ControladorDAO;
import com.lauralucena.pizzeria.pojos.Pizza;

public class PedidosActivity extends BaseActivity {
    private Button btnFavorita, btnCarta, btnAlGusto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedidos);

        this.btnFavorita = findViewById(R.id.btnFavorita);
        this.btnCarta = findViewById(R.id.btnNuestrasPizzas);
        this.btnAlGusto = findViewById(R.id.btnPizzaGusto);


        this.btnFavorita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Pizza pizzaFavSaved = ControladorDAO.getPizzaFavorita(getApplicationContext());

                if(pizzaFavSaved != null ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(PedidosActivity.this);
                    builder.setMessage("¿Desea pedir de nuevo su favorita? \nIngredientes: " + pizzaFavSaved.getIngredientesStringFromPizza() +
                            "\nTamaño: " + pizzaFavSaved.getTamPizza() +
                            "\nPrecio: " + pizzaFavSaved.getPrecioPizza() )
                            .setTitle("INFORMACIÓN");


                    //Botón SI
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(PedidosActivity.this, ConfirmacionPedidoActivity.class);
                            intent.putExtra("pedidoPizza",  pizzaFavSaved);
                            startActivity(intent);
                        }
                    });
                    //Botón NO
                    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {}
                    });
                    //Mostramos
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }

            }
        });


        this.btnCarta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PedidosActivity.this, ListadoPizzasActivity.class);
                startActivity(intent);
            }
        });



        this.btnAlGusto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PedidosActivity.this, PizzaPersonalizadaActivity.class);
                startActivity(intent);
            }
        });
    }
}