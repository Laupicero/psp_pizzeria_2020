package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.lauralucena.pizzeria.R;

public class LoginActivity extends BaseActivity {
    private EditText userName, userPass;
    private CheckBox cbSaveUserData;
    private Button btnLogin, btnSinLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

        //Bindings
            this.userName = findViewById(R.id.userNameLogin);
            this.userPass = findViewById(R.id.userPassLogin);
            this.cbSaveUserData = findViewById(R.id.userSaveData);
            this.btnLogin = findViewById(R.id.btnLogin);
            this.btnSinLogin = findViewById(R.id.btnSinLogin);



        //Acción BOTÓN-LOGIN
        this.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Si queremos guardar nuestros datos antes debemos comprobar
                // si está seleccionado el checkBox
                if(cbSaveUserData.isChecked()){
                    //Comprobamos que hay algo escrito en los EditText
                    if(!userName.getText().toString().isEmpty() && !userPass.getText().toString().isEmpty()){
                        SharedPreferences.Editor editorPrefs = prefs.edit();
                        editorPrefs.putString("nombreUsuario", userName.getText().toString());
                        editorPrefs.putString("passUsuario", userPass.getText().toString());
                        editorPrefs.apply();

                        Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Toast.makeText(getApplicationContext(), "Escriba tanto el usuario como la contraseña", Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(), "Seleccione primero 'Recordar Usuario'", Toast.LENGTH_LONG).show();
                }
            }
        });

        //BOTÓN SIN-LOGIN
        this.btnSinLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }


    // Para sobreEscribir el botón atrás
    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_message)
                .setTitle(R.string.dialog_title);
        //Botón SI
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button
                finishAffinity();
            }
        });


        //Botón NO
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {}
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}