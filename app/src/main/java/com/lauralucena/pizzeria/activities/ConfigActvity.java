package com.lauralucena.pizzeria.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.lauralucena.pizzeria.R;

public class ConfigActvity extends BaseActivity {
    private Spinner spinnerFondo;
    private Button btnDeslogueo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        this.spinnerFondo = findViewById(R.id.spinnerFondo);
        this.btnDeslogueo = findViewById(R.id.btnDeslogueo);

        // Opciones Spinner
        String[] temas = {"Tema Rosa","Tema Verde"};
        // Rellenamos el spinner con un Array
        this.spinnerFondo.setAdapter(new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, temas));


        //--------------------
        // EVENTOS
        //--------------------

        //Evento de cambio del Spinner
        this.spinnerFondo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
                SharedPreferences.Editor editorPrefs = prefs.edit();
                editorPrefs.putString("tema", spinnerFondo.getSelectedItem().toString());
                editorPrefs.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}