package com.lauralucena.pizzeria.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.lauralucena.pizzeria.R;

abstract public class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        String tenaAxtual = prefs.getString("tema", "Tema Rosa");

        // Para cambiar el Tema
        if(tenaAxtual.equalsIgnoreCase("Tema Rosa"))
            setTheme(R.style.Theme_Pizzeria);
        else
            setTheme(R.style.Theme_PizzeriaGreen);
    }
}
