package com.lauralucena.pizzeria.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.lauralucena.pizzeria.R;

public class SplashActivity extends BaseActivity {
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        prefs =  getSharedPreferences("MisPreferencias", getApplicationContext().MODE_PRIVATE);

        Handler handler = new Handler(getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(!prefs.getString("nombreUsuario", "user").equals("user")) {
                    Intent intent = new Intent(SplashActivity.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }, 2500);

        //-------------------------------
        //Vemos si hay preferencias guardadas anteriormente
        // Si las hubiese, no cargamos esta página
        /*if(prefs.getString("nombreUsuario", "user").equals("user")){
            Intent intent = new Intent(LoginActivity.this, MenuActivity.class);
            startActivity(intent);
            finish();

        }*/

    }
}