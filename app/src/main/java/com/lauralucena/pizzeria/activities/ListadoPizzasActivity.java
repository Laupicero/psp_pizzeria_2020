package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.adapter.PizzaAdapter;
import com.lauralucena.pizzeria.controllers.ControladorDAO;
import com.lauralucena.pizzeria.pojos.Pizza;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ListadoPizzasActivity extends BaseActivity {

    private RecyclerView rvPizzas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listado_pizzas);

        rvPizzas = findViewById(R.id.recViewPizzas);

        //recuperamos el listado de pizzas y montamos el adapter
        PizzaAdapter adapter = new PizzaAdapter(ControladorDAO.getListadoPizzas(getApplicationContext()));
        adapter.setListener(new PizzaAdapter.OnClickPizzaListener() {
            @Override
            public void onItemClicked(Pizza pizzaItem) {
                //Toast.makeText(ListadoPizzasActivity.this, item.getNombrePizza(), Toast.LENGTH_LONG).show();

                if (pizzaItem != null) {
                    Intent actFinPedido = new Intent(ListadoPizzasActivity.this, PizzaDataActivity.class);actFinPedido.putExtra("pedidoPizza", pizzaItem);
                    actFinPedido.putExtra("pedidoPizza", pizzaItem);

                    startActivity(actFinPedido);
                } else {
                    Toast toastmsj = Toast.makeText(getApplicationContext(), "ERROR INESPERADO", Toast.LENGTH_LONG);
                    toastmsj.show();
                }
            }
        });

        rvPizzas.setAdapter(adapter);
    }
}