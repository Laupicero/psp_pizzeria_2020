package com.lauralucena.pizzeria.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.pojos.Pizza;

public class ConfirmacionPedidoActivity extends BaseActivity {
    private Button btnVolverMenu;
    private TextView tvDatosPedido;
    private Pizza pedidoPizza;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmacion_pedido);

        this.tvDatosPedido = findViewById(R.id.tvDatosPedido);
        this.btnVolverMenu = findViewById(R.id.btnVolverMenu);
        this.pedidoPizza = (Pizza) getIntent().getSerializableExtra("pedidoPizza");

        this.tvDatosPedido.setText("Su pizza " + this.pedidoPizza.getNombrePizza() + " estará lista en 45min." +
                "\nSu pedido costará " + this.pedidoPizza.getPrecioPizza() + "€");

        this.btnVolverMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmacionPedidoActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}