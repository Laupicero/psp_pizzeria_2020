package com.lauralucena.pizzeria.controllers;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.lauralucena.pizzeria.models.DAO_Pizza_SQLite;
import com.lauralucena.pizzeria.pojos.Pizza;
import java.util.List;

public class ControladorDAO {

    // Obtenemos el listado de las Pizzas
    public static List<Pizza> getListadoPizzas(Context context){
        return DAO_Pizza_SQLite.getInstance(context).getListadoPizzas();
        // DE DAOPIZZA Sin DB:
        //return DAOPizza.getInstance().getListaPizzas();
    }



    // Al pasarle una pizza por parámetro, nos devolverá un string con todos
    // sus ingrediente para poder visualizarlas en un textView
    public static String getIngredientesFromAPizza(Pizza pizza) {
        return pizza.getIngredientesStringFromPizza();
    }

    // Para guardar nuestra instancia de Pizza en las preferencias
    public static void guardarPizzaFavorita(Context context, Pizza pizzaFav) {
        Gson gson = new Gson();
        String json = gson.toJson(pizzaFav);
        SharedPreferences mPrefs = context.getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString("miPizzaFavorita", json);
        prefsEditor.apply();
    }

    // Para obtener nuestra instancia de Pizza de nuestras preferencias
    public static Pizza getPizzaFavorita(Context context){
        SharedPreferences mPrefs = context.getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString("miPizzaFavorita", null);
        return gson.fromJson(json, Pizza.class);
    }
}
