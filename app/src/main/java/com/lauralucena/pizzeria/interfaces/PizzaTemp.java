package com.lauralucena.pizzeria.interfaces;

public interface PizzaTemp {
    double getPrecioPizza();
}
