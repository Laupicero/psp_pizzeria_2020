package com.lauralucena.pizzeria.models;

import com.lauralucena.pizzeria.R;
import com.lauralucena.pizzeria.pojos.Ingrediente;
import com.lauralucena.pizzeria.pojos.Pizza;

import java.util.ArrayList;
import java.util.List;

public class DAOPizza {
    //Es un Singleton así que el constructor es privado
    private List<Pizza> listaPizzas;
    private static DAOPizza pizzaSingleton;


    //Constructor
    // Nos creamos las pizzas Predefinidas
    // Todas tendrán por defecto, un tamaño Mediano
    private DAOPizza(){
        this.listaPizzas = new ArrayList<Pizza>();
        listaPizzas.add(new Pizza("Carbonara", "Mediano", R.drawable.carbonara, getIngredientesDAOPizza("Huevo", "Nata", "Bacon")));
        listaPizzas.add(new Pizza("Margarita", "Mediano", R.drawable.margarita, getIngredientesDAOPizza("Tomate", "Queso", "Oregano")));
        listaPizzas.add(new Pizza("Tunara", "Mediano", R.drawable.tunara, getIngredientesDAOPizza("Atun", "Tomate", "Queso")));
        listaPizzas.add(new Pizza("Mexicana", "Mediano", R.drawable.mexicana, getIngredientesDAOPizza("Chorizo-picante", "Tomate", "Guindilla")));
        listaPizzas.add(new Pizza("Peperonni", "Mediano", R.drawable.peperonni, getIngredientesDAOPizza("Huevo-batido", "Peperonni", "Pimiento")));
        listaPizzas.add(new Pizza("Capricciossa", "Mediano", R.drawable.capricciossa, getIngredientesDAOPizza("Huevo", "Queso-Mimole", "Jamon")));
        listaPizzas.add(new Pizza("Especial", "Mediano", R.drawable.especial, getIngredientesDAOPizza("Ternera", "Queso-Harvatti", "Bacon")));
        listaPizzas.add(new Pizza("Carnivora", "Mediano", R.drawable.carnivora, getIngredientesDAOPizza("Bacon", "Ternera", "Salsa-bbq")));
    }

    //GETTER
    public List<Pizza> getListaPizzas() { return listaPizzas;}

    //Singleton que llama al constructor
    public static DAOPizza getInstance(){
        if (pizzaSingleton == null)
            pizzaSingleton = new DAOPizza();

        return pizzaSingleton;
    }


    //Metodo que nos crea/devuelve una Lista de ingredientes para las pizzas predeterminadas
    private List<Ingrediente> getIngredientesDAOPizza(String ingred1, String ingred2, String ingred3){
        List<Ingrediente> lstIngredientes = new ArrayList<Ingrediente>();
        //Creamos los ingredientes
        Ingrediente ingrediente1 = new Ingrediente(ingred1);
        Ingrediente ingrediente2 = new Ingrediente(ingred2);
        Ingrediente ingrediente3 = new Ingrediente(ingred3);
        //Añadimos los ingredientes a la lista
        lstIngredientes.add(ingrediente1);
        lstIngredientes.add(ingrediente2);
        lstIngredientes.add(ingrediente3);
        //Devolvemos el listado
        return  lstIngredientes;
    }
}
